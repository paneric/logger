<?php

declare(strict_types=1);

namespace Paneric\Logger;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class LoggerFactory
{
    private $path;
    private $level;
    private $filePermission;

    public function __construct(array $settings)
    {
        $this->path = (string)$settings['path'];
        $this->level = (int)$settings['level'];
        $this->filePermission = $settings['file_permission'];
    }

    private $handler = [];

    public function createInstance(string $name): LoggerInterface
    {
        $logger = new Logger($name);

        foreach ($this->handler as $handler) {
            $logger->pushHandler($handler);
        }

        $this->handler = [];

        return $logger;
    }

    public function addFileHandler(string $filename, int $level = null, int $filePermission = null): self
    {
        $filename = sprintf('%s/%s', $this->path, $filename);

        $rotatingFileHandler = new RotatingFileHandler(
            $filename,
            0,
            $level ?? $this->level,
            true,
            $filePermission ?? $this->filePermission
        );

        // The last "true" here tells monolog to remove empty []'s
        $rotatingFileHandler->setFormatter(
            new LineFormatter(
                null,
                null,
                false,
                true
            )
        );

        $this->handler[] = $rotatingFileHandler;

        return $this;
    }

    public function addConsoleHandler(int $level = null): self
    {
        $streamHandler = new StreamHandler('php://stdout', $level ?? $this->level);
        $streamHandler->setFormatter(
            new LineFormatter(
                null,
                null,
                false,
                true
            )
        );

        $this->handler[] = $streamHandler;

        return $this;
    }
}
